# Obsidian
1. Install, features section of website.
2. Initial overview of the interface
3. Opening Help (Online help: https://publish.obsidian.md/help/Index)
4. Settings:
	1. Plugins
	2. **Hotkeys**
	3. Templates
	4. Daily Notes (Why not publish and sync?)
5. Writing Notes
	1.  Basic GFM (https://publish.obsidian.md/help/How+to/Format+your+notes) and Folding (Hint, shift+tab and tab will renumber lists)
	2. Linking and Tagging
		1. Internal (https://publish.obsidian.md/help/How+to/Internal+link) [[Block Link#Heading to link to]]
		2. Alias https://publish.obsidian.md/help/How+to/Add+aliases+to+note [[Block Link|Artificial Intelligence]]
		3. Blocks (https://publish.obsidian.md/help/How+to/Link+to+blocks) [[Block Link#^c9a575]]
	3. Moving
	4. Daily
	5. Star
	6. Search (cmd F, Global, Vim)
	7. Audio
	8. Write and Insert Template
	9. Writing Slideshows
6. Command Pallet
7. Quick Switcher and Templates
8. Links
9. Tags (And how I would use them)
10. Markdown Importer
11. Views
12. Windows, Tabs, Panes
13. Workspaces
14. Very simple vim, and why use it.


# Test Projects
1. Research Paper (Music Theory / Philosophy)
2. Project Management (Pizza / Coffee )

### Neither of the above!
•• MARKETING ••
• An overview of important metrics
• Social media
• eMail