# Diversity
It doesn't set right with me that people who ought to recognize individuals, instead attempt to fit everyone into a single category.

Code camps often each design, but separetly from development. Nearly none teach project management, product ownership, marketing, sales, content management, email systems, etc. Therefore, code camps are not preparing students for a holistic journey into the industry, but rather for a specific job function within that industry (react developer is very common).

Inclusion is how we make diversity work. Without inclusion, diversity is nothing more than a checkbox. Code camps ought to include more holistic approaches to the industry than training for a single job function.

# The Guitar Shaped Dust Shelf

I thought for a long time I would become a guitar player. I could somewhat grasp the idea of turning the notes of a scale into a phrase. I could tell you the name of a chord based on the intervals. Some of those chords I could actually play. That's where I got after about ten years of practice.

I am not a guitar player. And that's fine.

Moral of this story is: Doing isn't being. And that's not simply semantics.

# Respect For Special Needs

At camp, I got the opportunity to learn and work with a magnificently bright author, and autist. The man wrote more than Jordan shot hoops. His coursework would often suffered because of his passion for writing.

I got the chance to ask once, "Why be a dev, you love to write, you know so much, you've got actual fans that follow you, why do the development career instead?"

In short, he replied "Because that's what people tell me I can do for money."

Perhaps I shouldn't have, but not planting the seed was unacceptable in my mind, so, I pushed a little. 

"What about write?"

"For Money?"

"Yeah, bloggers, novelists, columnists, critics, copywriters, lots of writers in the world"

We wen't on a bit. I planted the seed I though I needed to. It'll grow, or it won't. It's out of my hands and at least into fertile soil. I did what I could, and I genuinely hope he finds the truth of this matter, and the happiness he deserves.

For code camps to refuse this person, or anyone, their passion in place of a falsehood, is straight fucked up.

# The Code Camp Faculty Trap

While I was in camp, recent graduates would be offered faculty positions and then train the next batch of students to be ... faculty. 

Code camp instructor isn't the same job as web developer. Full stop. Doing the same curriculum a second, third, fourth time, doesn't improve ones skillet. Remember that guitar I used to 'practice?'  Walking up and down the same scale over and over, never makes one more creative, and never teaches one something new. Doing the same assignments on repeat is no different.

# Conclusion

Positivity is great and we should encourage one another, but we should not fail to consider the individual.

Doing so means we ought to reject the idea that everyone can be placed into a single category, even if that category is 'developer.'