# Maintinance Notes

## Image Links
Image links in markdown files must be of the form:
image: "https://dsbarnes.gitlab.io/hugo-dev-blog/icons/dev-blog-favicon.ico"

## Share Links
If you want to share on pinterest this will work:
Not sure if that's usefull for a dev-blog, but avaliable

```html
{{ if .Params.image }}
<div class="pinterest" title="Share this on Pinterest" onclick="window.open(
    'https://pinterest.com/pin/create/button/?url=&media={{ .Params.image }}&description={{ .Description }}');">
    <img src="{{ site.BaseURL }}/icons/social/icon-pinterest-share.svg" alt="">
</div>
{{ end }}
```

## Post Images