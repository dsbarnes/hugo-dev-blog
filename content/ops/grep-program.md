---
name: "Grep Program"
title: "The Grep Program"
description: "Grep is one of the most useful programs ever written."
date: 2021-03-14

images: ['https://dsbarnes.gitlab.io/hugo-dev-blog/post-images/.png']

series: ["Power Tools"]
category: "Operations"
tags: ""

draft: true
---

Grep is one of the most useful programs ever written.\

Here are some ways you can start using it today.
