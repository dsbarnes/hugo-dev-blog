---
name: "CMSs Are Awesome"
title: "CMSs Are Awesome"
description: ""
date: 2021-03-29

video: 'https://www.youtube.com/embed/w7Ft2ymGmfc'
images: ['https://dsbarnes.gitlab.io/hugo-dev-blog/post-images/.png']

series: ["Self Aware Shitpost"]
category: "Content Management Systems"
tags: "Shitpost"

draft: true
---

The modern content management systems that exist today are stone ballers. I fuck with WordPress **and** Shopify. I've never used Squarespace, Wix, or Webflow, but have no reason to doubt they're awesome. Hell, I suspect Magento is worth a venture.

This ~~fact~~ opinion of mine is somehow overlooked by top code camps, with a couple of exceptions here and there.

That's dumb as hell.

Say I'm at a code camp right, and this kid has ambitions for his projects, and he mentions his dad is calling him up all the time asking about how he might get this or that started for his website, and what a bother this is. 

I ask him why? Charge the man. 

Can't charge the man it's my dad, besides he's already got good business, niche market, whatever, whatever.

Good business? Woe nelly! What's he do?

Costumes.

Yea, that's pretty seasonal.

No, like civil war reenactment costumes.

No shit? And why don't you build him the site? He's got half the business, you take ad revenue for managing or something, I don't know man, seems to me if he knows what time it is, that's the value, drop this baseball card...er, whatever project you're doing and jump on that.

Na, you want to swing at it?

Yes.

His number.

Email?

No site yet.

No Email at all? Gmail?

# Good luck.

Didn't need it.

See, there's this thing called, say it with me, Shopify.

Stores need some level of inventory, yeah? And will thereby need to manage said inventory. Stores need to keep track of customer emails, phone, shipping info. They would like to be able to easily integrate marketing campaigns. And that would mean make discount codes and such. Designing an analytics dashboard, tracking the strength of said campaigns, and integrations, and make sure each customer is getting the appropriate emails, oh and a CRM... 

Damn, that's a lot to build.

It's also included, all of that, out of the box with Shopify.

So why would anyone rebuild all of that?

And why are people graduating code camp not knowing about Liquid templates?
Freelancers
I don't mean those students who did some stupid shit and got tossed out of class for anything other than honorable mentions, or the ones who tried really hard and didn't make the grade, so on LinkedIn they mark their profession as 'freelance developer.' Look, if you don't have a template for invoicing, you're very likely not a freelance anything. If you haven't considered making one of those, you're for sure not a freelance anything. 

Pro Tip: Stop doing that, it's cartoonishly easy to call you out, and it guarantees you don't get the job, because we cannot trust your lying ass.

I mean the people who go out and build a portfolio of actual work for actual clients. Them folk use a metric shit ton of WordPress.

Know why? It's rapid. It's easy. You can hire a bonehead for a subcontractor and still get fine results, and just as Shopify comes with some mess of madness that you'd never build custom in your life out of the box.

But React and Django are shiny and fancy, and look at all the GitHub stars.

Don't get me wrong, those technologies are boss af, but how many jobs do you think there are for junior devs in comparison? Seriously, junior Django jobs? Where do you live there are junior Django jobs? 

WordPress is probably going to afford one the opportunity to learn some React or Vue, or whatever frontend. I used backbone once. Fuck backbone. The same can be said for headless Shopify.

If the school you are considering says they teach modern frameworks and methodologies, but nothing in the curriculum pertains to WordPress or Shopify, that's suspicious.

If they have a career preparation program that includes extensive amounts of networking and soft skills, good, I can forgive them for passing on the CMSs. 

Beyond a focus on career prep, I don't know of a reasonable excuse.

Camps will argue that the curriculum is designed for students to 'learn how to learn,' and since such CMSs have such a large community and so much documentation, graduates would have no problem picking such things up.

True.

Silly, but true. You don't need to understand much at all about programming to figure out WordPress or Shopify themes. A plugin v.s. a package ecosystem is pretty different. They tend to live together. Why would it hurt to know about how CMSs handle the database? Or the problems one might encounter with their version control as a result of the way said CMSs handle the database? (Looking at you Elementor). WordPress multisite. cPanel is the most popular server control panel in the world. The concept of managed hosting ... I mean shit, there is quite a lot here that isn't difficult to pick up. So why pass? That's silly.

None of this, by the way, would be hard to add alongside JS curriculums that already exists. A simple headless setup would do.
